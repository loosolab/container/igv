FROM node:latest

RUN apt-get update && apt-get install -y git 

RUN git clone https://github.com/igvteam/igv-webapp.git

WORKDIR ./igv-webapp

RUN npm cache clean --force

RUN npm install 

RUN npm run build

EXPOSE 80
