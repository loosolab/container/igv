var igvwebConfig = {

    genomes: "https://s3.mpi-bn.mpg.de/igv-references/genomes.json",
    trackRegistryFile: "https://s3.mpi-bn.mpg.de/igv-references/trackRegistry.json",
    sessionRegistryFile: "https://s3.mpi-bn.mpg.de/igvdata-bn-test-cutnrun-seq/mySessionRegistry.json",

    // Supply a drobpox api key to enable the Dropbox file picker in the load menus.  This is optional
    //dropboxAPIKey: "...",

    // Supply a Google client id to enable the Google file picker in the load menus.  This is optional
    //clientId: "...",
    // apiKey: "...",

    // Provide a URL shorterner function or object.   This is optional.  If not supplied
    // sharable URLs will not be shortened .
    urlShortener: {
        provider: "tinyURL"
    },

    enableCircularView: true,

    igvConfig:
        {
            genome: "hg19",
            locus: "all",
            genomeList: "https://s3.mpi-bn.mpg.de/igv-references/genomes.json",
            queryParametersSupported: true,
            showChromosomeWidget: true,
            showSVGButton: false,
            tracks: []
        }

}
