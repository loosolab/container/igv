<<<<<<< HEAD
![build](https://github.com/igvteam/igv-webapp/actions/workflows/ci_build.yml/badge.svg)
[![](https://img.shields.io/github/last-commit/igvteam/igv-webapp.svg)](https://github.com/igvteam/igv-webapp)

# IGV-Web App

The IGV-Web app is a pure-client "genome browser" application based on [igv.js](https://github.com/igvteam/igv.js).  It is developed by the [Integrative Genomics Viewer (IGV)](https://igv.org) team. You can use our hosted app at https://igv.org/app, or follow the directions to install your own.

## Supported Browsers

The IGV-Web app and igv.js require a modern web browser with support for JavaScript ECMAScript 2015.

## Using IGV-Web

For documentation about using the app, see [https://igv.org/doc/webapp/](https://igv.org/doc/webapp/). A link to the user documentation is also provided in the app's Help menu.

## Hosting your own IGV-Web app

For documentation about hosting your own IGV-Web, see [https://igv.org/doc/webapp/#Hosting/](https://igv.org/doc/webapp/#Hosting/)


## License
The IGV-Web app is [MIT](/LICENSE) licensed.

=======
# IGV Container

Container image that is deployed by the BCU-Repository

## Resources

Reference genomes that can be selected via the the top left `Genome` are taken from a bucket on our s3 server: [https://s3.mpi-bn.mpg.de/igv-references/](https://s3.mpi-bn.mpg.de/igv-references/)

If you want to add a new reference genome to it you have to add an entry to the `genomes.json` file [**in this bucket**](https://s3.mpi-bn.mpg.de/igv-references/genomes.json). Go to the bottom of the file, and within the outmost brackets [ ], add the entry in the form of this template:
```
{
        "id": "<REFERENCE GENOME ID>",
        "name": "<REFERENCE GENOME NAME> (<REFERENCE GENOME ID>)",
        "fastaURL": "https://s3.mpi-bn.mpg.de/igv-references/<FASTA>",
        "indexURL": "https://s3.mpi-bn.mpg.de/igv-references/<FASTA_INDEX>",
        "order": 1000000,
        "tracks": [
          {
            "name": "Genes",
            "url": "https://s3.mpi-bn.mpg.de/igv-references/<GTF>",
            "order": 1000000,
            "removable": false,
            "visibilityWindow": -1
          }
        ]
      }
``` 
Additional tracks can be added if needed, but these should always be present.

After you modified the `genomes.json` file, you have to upload this updated version, as well as the files (`.fa`,` .fa.fai`, `.gtf`) to the s3 bucket.

If you have done all of these step correctly, the genome should show up at the bottom of the `Genome` tab and after selections, the tracks will be displayed.
>>>>>>> master
